package com.example.rgi_40.retrofitdemo.retrofit;

import com.example.rgi_40.retrofitdemo.model.UserList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by rgi-40 on 24/1/18.
 */

public interface ApiList {
    @GET("api/users")
    Call<UserList> getAllUserList(@Query("page") String page_number);
}
