package com.example.rgi_40.retrofitdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.rgi_40.retrofitdemo.model.UserList;
import com.example.rgi_40.retrofitdemo.retrofit.ApiList;
import com.example.rgi_40.retrofitdemo.retrofit.ApiService;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ApiService.changeApiBaseUrl("https://reqres.in");
        ApiService.createService(ApiList.class).getAllUserList("2").enqueue(
                new Callback<UserList>() {
                    @Override
                    public void onResponse(Call<UserList> call, Response<UserList> response) {
                        Log.e("url", call.request().url().toString());
                        if (response.isSuccessful()) {
                            UserList reviewRateListDataProduct = response.body();
                            Log.e("data", reviewRateListDataProduct.toString());
                        } else {
                            /**
                             * Handle error by getting raw data
                             * To get raw data use,
                             * response.errorBody().string()
                             */
                            try {
                                Log.e("error", response.errorBody().string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<UserList> call, Throwable t) {
                        Log.e("url", call.request().url().toString());
                        printErrorLog("error", t);
                    }
                }
        );
    }

    protected void printErrorLog(String log, Throwable t) {
        if (t != null && t.getMessage() != null) {
            Log.e(log, t.getMessage());
        }
    }
}
;