package com.lim.splitexpense;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Vishal Rathod on 14/9/17.
 */

public class BaseFragment extends Fragment implements View.OnClickListener {
    protected FragmentManager mManager;
    protected FragmentTransaction mTransition;
    protected Context mContext;
    private Typeface typeface;
    protected View rootView;

    private TextView txtTitle;
    private TextView txtAdd;
    private ImageView imgBack;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mManager = getFragmentManager();
        mTransition = mManager.beginTransaction();
    }

    protected void setTitle(String title) {
        txtTitle = rootView.findViewById(R.id.txtTitle);
        setTypeface(txtTitle, "fonts/" + getString(R.string.font_lato));

        txtTitle.setText(title);
    }

    protected void displayAddButton(boolean value) {
        txtAdd = rootView.findViewById(R.id.txtAdd);
        setTypeface(txtAdd, "fonts/" + getString(R.string.font_quicksand));
        txtAdd.setOnClickListener(this);

        if (value) {
            txtAdd.setVisibility(View.VISIBLE);
        } else {
            txtAdd.setVisibility(View.GONE);
        }
    }

    protected void displayBackButton(boolean value) {
        imgBack = rootView.findViewById(R.id.imgBack);
        imgBack.setOnClickListener(this);

        if (value) {
            imgBack.setVisibility(View.VISIBLE);
        } else {
            imgBack.setVisibility(View.GONE);
        }
    }

    protected void setTypeface(View view, String fontType) {
        typeface = Typeface.createFromAsset(mContext.getAssets(),
                fontType);
        if (view instanceof EditText) {
            ((EditText) view).setTypeface(typeface);
        } else if (view instanceof TextView) {
            ((TextView) view).setTypeface(typeface);
        } else if (view instanceof Button) {
            ((Button) view).setTypeface(typeface);
        }
    }

    @Override
    public void onClick(View view) {

    }
}

